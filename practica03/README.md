# Leonardo Gallo Guerrero

## Práctica 3

### Ejercicio 1 : esquema de intercambio de llaves Diffie-Hellman

El escenario es el siguiente, A y B, sin contacto 
previo, quieren establecer un  intercambio de mensajes, el 
medio es un canal de comunicación inseguro, la pregunta que 
nos resuelven Diffie-Hellman es ¿Cómo volver seguro el 
intercambio de mensajes? es decir resolver el problema de 
codificación de información sobre canales públicos.

La idea es crear un sistema de llaves, públicas y 
privadas, donde una es compartida. Dicho sistema se basa en un 
algoritmo de codificación que permite a ambas partes construir una 
llave común la cual intentar descifrar resulta muy costoso 
computacionalmente, esto hace que la comunicación se vuelva segura.

El algorimo:
+ Espablecer un primo p y un elemento g en 
las clases residuales módulo p menos el cero ( Z<sub>p</sub>^* ). 
Estos datos son conocidos, es decir públicos.
+ A y B escogen aleatoriamente un elemento a y b, respectivamente, 
en las clases residuales módulo p-1 (Z<sub>p-1</sub>) y calculan 
A 
-> KeyA = g^a mod p y B -> KeyB = g^b mod p, envían el resultado a su 
contraparte, llaves públicas.
+ Usando las propiedades de grupo en Z<sub>p</sub> podermos ver que 
con la información que cuenta A y B se puede calcular PrivKey = 
g^ab mod p, y esto vendría siendo nuestra llave compartida.

Obsercaciones:
+ Siendo un atacante se tiene también conocimiento de las llaves 
públicas sin embargo para poder obtener la llave PrivKey
necesitaria tener los valores a o b y obtenerlos a partir de  KeyA 
y KeyB es un problema que se cree intratable computacionalmente.
